Description: Migrate away from deprecated json-simple 1.x classes
 See json-simple 2.0.0 changelog:
 > * Deprecated JSONParse and JSONValue in favor of Jsoner.
 > * Deprecated JSONStreamAware and JSONAware in favor of Jsonable.
 > * Deprecated JSONObject in favor of JsonObject.
 > * Deprecated JSONArray in favor of JsonArray.
 .
 This patch uses the new json-simple Json* classes. It is compatible with
 both 2.x and 3.x json-simple releases, with a few ajustments regarding
 backward incompatible changes in json-simple 3.x:
 - The package name, changed to com.github.cliftonlabs.json_simple
 - The exception DeserializationExcetpion renamed as JsonException
 These two changes are handled using place-holders @JSON_SIMPLE_PACKAGE@
 and @JSON_EXCETPION@ which are substituted at build time by debian/rules.
 .
 With these tricks the package is compatible with json-simple 2.x and 3.x.
 .
 Sebastien Jodogne <s.jodogne@orthanc-labs.com> has added a few fixes to
 the original patch.
Author: Gilles Filippini <pini@debian.org>
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
Index: OrthancImageJ-1.2/com/orthancserver/DicomDecoder.java
===================================================================
--- OrthancImageJ-1.2.orig/com/orthancserver/DicomDecoder.java
+++ OrthancImageJ-1.2/com/orthancserver/DicomDecoder.java
@@ -28,7 +28,8 @@ import ij.process.ShortProcessor;
 import ij.process.ColorProcessor;
 import ij.io.FileInfo;
 import ij.measure.Calibration;
-import org.json.simple.*;
+import @JSON_SIMPLE_PACKAGE@.JsonObject;
+import @JSON_SIMPLE_PACKAGE@.JsonArray;
 import java.util.List;
 import java.util.ArrayList;
 import java.util.Collections;
@@ -92,10 +93,10 @@ public class DicomDecoder
   };
 
   private static void ExtractCalibration(ImagePlus image,
-                                         JSONObject tags)
+                                         JsonObject tags)
   {
-    JSONObject rescaleIntercept = (JSONObject) tags.get("0028,1052");
-    JSONObject rescaleSlope = (JSONObject) tags.get("0028,1053");
+    JsonObject rescaleIntercept = (JsonObject) tags.get("0028,1052");
+    JsonObject rescaleSlope = (JsonObject) tags.get("0028,1053");
     if (rescaleIntercept != null &&
         rescaleSlope != null)
     {
@@ -108,9 +109,9 @@ public class DicomDecoder
   }
 
   private static void ExtractPixelSpacing(ImagePlus image,
-                                          JSONObject tags)
+                                          JsonObject tags)
   {
-    JSONObject pixelSpacing = (JSONObject) tags.get("0028,0030");
+    JsonObject pixelSpacing = (JsonObject) tags.get("0028,0030");
     if (pixelSpacing != null)
     {
       String[] tokens = ((String) pixelSpacing.get("Value")).split("\\\\");
@@ -130,7 +131,7 @@ public class DicomDecoder
   }
 
   private static void ExtractDicomInfo(ImagePlus image,
-                                       JSONObject tags)
+                                       JsonObject tags)
   {
     String info = new String();
 
@@ -143,7 +144,7 @@ public class DicomDecoder
     Collections.sort(tagsIndex);
     for (String tag : tagsIndex) 
     {
-      JSONObject value = (JSONObject) tags.get(tag);
+      JsonObject value = (JsonObject) tags.get(tag);
 
       if (((String) value.get("Type")).equals("String"))
       {
@@ -232,7 +233,7 @@ public class DicomDecoder
 
 
   private String[]  SortSlicesBy3D(OrthancConnection c, 
-                                   JSONArray instances) throws IOException
+                                   JsonArray instances) throws IOException
   {
     ArrayList<Slice> slices = new ArrayList<Slice>();
     float normal[] = null;
@@ -243,7 +244,7 @@ public class DicomDecoder
     for (int i = 0; i < instances.size(); i++)
     {
       String uuid = (String) instances.get(i);
-      JSONObject instance = (JSONObject) c.ReadJson("/instances/" + uuid + "/tags?simplify");
+      JsonObject instance = (JsonObject) c.ReadJson("/instances/" + uuid + "/tags?simplify");
       if (!instance.containsKey("ImageOrientationPatient") ||
           !instance.containsKey("ImagePositionPatient"))
       {
@@ -298,16 +299,21 @@ public class DicomDecoder
 
 
   private String[]  SortSlicesByNumber(OrthancConnection c, 
-                                       JSONArray instances) throws IOException
+                                       JsonArray instances) throws IOException
   {
     ArrayList<Slice> slices = new ArrayList<Slice>();
 
     for (int i = 0; i < instances.size(); i++)
     {
       String uuid = (String) instances.get(i);
-      JSONObject instance = (JSONObject) c.ReadJson("/instances/" + uuid);
-      Long index = (Long) instance.get("IndexInSeries");
-      slices.add(new Slice((float) index, uuid));
+      JsonObject instance = (JsonObject) c.ReadJson("/instances/" + uuid);
+
+      // Addition by Sebastien Jodogne
+      if (instance.containsKey("IndexInSeries"))
+      {
+	  String index = instance.get("IndexInSeries").toString();  // This is a java.math.BigDecimal
+	  slices.add(new Slice(Float.parseFloat(index), uuid));
+      }
     }
 
     return SortSlices(slices);
@@ -316,7 +322,7 @@ public class DicomDecoder
 
 
   private String[] GetSlices(OrthancConnection c, 
-                             JSONArray instances) throws IOException
+                             JsonArray instances) throws IOException
   {
     String[] result;
 
@@ -345,30 +351,30 @@ public class DicomDecoder
                       String uuid) throws IOException, InterruptedException, ExecutionException
   {
     ImageStack stack = null;
-    JSONObject tags = null;
+    JsonObject tags = null;
     String tagsUri, name;
 
     if (isInstance)
     {
       name = "Instance " + uuid;
-      tags = (JSONObject) c.ReadJson("/instances/" + uuid + "/tags");
+      tags = (JsonObject) c.ReadJson("/instances/" + uuid + "/tags");
       stack = AddSlice(stack, c, uuid);
     }
     else
     {
       name = "Series " + uuid;
 
-      JSONObject series = (JSONObject) c.ReadJson("/series/" + uuid);
-      JSONArray instances = (JSONArray) series.get("Instances");
+      JsonObject series = (JsonObject) c.ReadJson("/series/" + uuid);
+      JsonArray instances = (JsonArray) series.get("Instances");
 
       try
       {
-        tags = (JSONObject) c.ReadJson("/series/" + uuid + "/shared-tags");
+        tags = (JsonObject) c.ReadJson("/series/" + uuid + "/shared-tags");
       }
       catch (Exception e)
       {
         // Fallback for old versions of Orthanc, without "shared-tags"
-        tags = (JSONObject) c.ReadJson("/instances/" + (String) instances.get(0) + "/tags");
+        tags = (JsonObject) c.ReadJson("/instances/" + (String) instances.get(0) + "/tags");
       }
 
       final String[] slices = GetSlices(c, instances);
Index: OrthancImageJ-1.2/com/orthancserver/OrthancConfigurationDialog.java
===================================================================
--- OrthancImageJ-1.2.orig/com/orthancserver/OrthancConfigurationDialog.java
+++ OrthancImageJ-1.2/com/orthancserver/OrthancConfigurationDialog.java
@@ -44,7 +44,7 @@ import javax.swing.JTree;
 import javax.swing.SwingWorker;
 import javax.swing.border.EmptyBorder;
 
-import org.json.simple.JSONObject;
+import @JSON_SIMPLE_PACKAGE@.JsonObject;
 
 
 public class OrthancConfigurationDialog extends JDialog 
@@ -110,7 +110,7 @@ public class OrthancConfigurationDialog
             OrthancConnection orthanc = CreateConnection();
             try
             {
-              JSONObject system = (JSONObject) orthanc.ReadJson("system");
+              JsonObject system = (JsonObject) orthanc.ReadJson("system");
               JOptionPane.showMessageDialog(null, "Successfully connected to this Orthanc server " +
                                             "(version: " + (String) system.get("Version") + ")!", 
                                             "Success", JOptionPane.INFORMATION_MESSAGE);
Index: OrthancImageJ-1.2/com/orthancserver/OrthancConnection.java
===================================================================
--- OrthancImageJ-1.2.orig/com/orthancserver/OrthancConnection.java
+++ OrthancImageJ-1.2/com/orthancserver/OrthancConnection.java
@@ -31,8 +31,9 @@ import java.net.URL;
 import java.net.URLConnection;
 import java.util.Base64;
 import javax.imageio.ImageIO;
-import org.json.simple.JSONObject;
-import org.json.simple.JSONValue;
+import @JSON_SIMPLE_PACKAGE@.JsonObject;
+import @JSON_SIMPLE_PACKAGE@.Jsoner;
+import @JSON_SIMPLE_PACKAGE@.@JSON_EXCEPTION@;
 
 public class OrthancConnection
 {
@@ -194,7 +195,13 @@ public class OrthancConnection
   public Object ReadJson(String uri) throws IOException
   {
     String content = ReadString(uri);
-    Object json = JSONValue.parse(content);
+    Object json;
+    try {
+        json = Jsoner.deserialize(content);
+    }
+    catch (@JSON_EXCEPTION@ e) {
+        json = null;
+    }
 
     if (json == null)
     {
@@ -222,9 +229,9 @@ public class OrthancConnection
     name_ = name;
   }
 
-  public JSONObject Serialize()
+  public JsonObject Serialize()
   {
-    JSONObject json = new JSONObject();
+    JsonObject json = new JsonObject();
     json.put("Name", name_);
     json.put("Url", baseUrl_);
 
@@ -236,7 +243,7 @@ public class OrthancConnection
     return json;
   }
 
-  public static OrthancConnection Unserialize(JSONObject json)
+  public static OrthancConnection Unserialize(JsonObject json)
   {
     OrthancConnection c = new OrthancConnection();
     c.SetInsecure(true);  // Fix issue 9 (cannot connect to self-signed certificates)
Index: OrthancImageJ-1.2/com/orthancserver/SelectImageDialog.java
===================================================================
--- OrthancImageJ-1.2.orig/com/orthancserver/SelectImageDialog.java
+++ OrthancImageJ-1.2/com/orthancserver/SelectImageDialog.java
@@ -21,9 +21,9 @@
 
 package com.orthancserver;
 
-import org.json.simple.JSONValue;
-import org.json.simple.JSONArray;
-import org.json.simple.JSONObject;
+import @JSON_SIMPLE_PACKAGE@.Jsoner;
+import @JSON_SIMPLE_PACKAGE@.JsonArray;
+import @JSON_SIMPLE_PACKAGE@.JsonObject;
 import java.io.IOException;
 import java.util.ArrayList;
 import java.util.Base64;
@@ -109,8 +109,8 @@ public class SelectImageDialog extends J
       {
         try
         {
-          JSONObject series = (JSONObject) orthanc_.ReadJson("series/" + uuid_);
-          JSONArray instances = (JSONArray) series.get("Instances");
+          JsonObject series = (JsonObject) orthanc_.ReadJson("series/" + uuid_);
+          JsonArray instances = (JsonArray) series.get("Instances");
           if (instances.size() > 0)
           {
             preview.Load(orthanc_, "/instances/" + instances.get(0) + "/preview");
@@ -150,12 +150,12 @@ public class SelectImageDialog extends J
     {
       List<MyTreeNode> children = new ArrayList<MyTreeNode>();
 
-      JSONArray patients = (JSONArray) orthanc_.ReadJson("patients");
+      JsonArray patients = (JsonArray) orthanc_.ReadJson("patients");
       for (int i = 0; i < patients.size(); i++)
       {
         String uuid = (String) patients.get(i);
-        JSONObject patient = (JSONObject) orthanc_.ReadJson("patients/" + uuid);
-        JSONObject main = (JSONObject) patient.get("MainDicomTags");
+        JsonObject patient = (JsonObject) orthanc_.ReadJson("patients/" + uuid);
+        JsonObject main = (JsonObject) patient.get("MainDicomTags");
 
         String s = new String();
         s = AddComponent(s, (String) main.get("PatientID"));
@@ -171,13 +171,13 @@ public class SelectImageDialog extends J
     {
       List<MyTreeNode> children = new ArrayList<MyTreeNode>();
 
-      JSONObject patient = (JSONObject) orthanc_.ReadJson("patients/" + uuid_);
-      JSONArray studies = (JSONArray) patient.get("Studies");
+      JsonObject patient = (JsonObject) orthanc_.ReadJson("patients/" + uuid_);
+      JsonArray studies = (JsonArray) patient.get("Studies");
       for (int i = 0; i < studies.size(); i++)
       {
         String uuid = (String) studies.get(i);
-        JSONObject study = (JSONObject) orthanc_.ReadJson("studies/" + uuid);
-        JSONObject main = (JSONObject) study.get("MainDicomTags");
+        JsonObject study = (JsonObject) orthanc_.ReadJson("studies/" + uuid);
+        JsonObject main = (JsonObject) study.get("MainDicomTags");
 
         String s = new String();
         s = AddComponent(s, (String) main.get("StudyDescription"));
@@ -193,13 +193,13 @@ public class SelectImageDialog extends J
     {
       List<MyTreeNode> children = new ArrayList<MyTreeNode>();
 
-      JSONObject study = (JSONObject) orthanc_.ReadJson("studies/" + uuid_);
-      JSONArray seriesSet = (JSONArray) study.get("Series");
+      JsonObject study = (JsonObject) orthanc_.ReadJson("studies/" + uuid_);
+      JsonArray seriesSet = (JsonArray) study.get("Series");
       for (int i = 0; i < seriesSet.size(); i++)
       {
         String uuid = (String) seriesSet.get(i);
-        JSONObject series = (JSONObject) orthanc_.ReadJson("series/" + uuid);
-        JSONObject main = (JSONObject) series.get("MainDicomTags");
+        JsonObject series = (JsonObject) orthanc_.ReadJson("series/" + uuid);
+        JsonObject main = (JsonObject) series.get("MainDicomTags");
 
         String s = new String();
         s = AddComponent(s, (String) main.get("Modality"));
@@ -215,24 +215,25 @@ public class SelectImageDialog extends J
     {
       List<MyTreeNode> children = new ArrayList<MyTreeNode>();
 
-      JSONObject series = (JSONObject) orthanc_.ReadJson("series/" + uuid_);
-      JSONArray instances = (JSONArray) series.get("Instances");
+      JsonObject series = (JsonObject) orthanc_.ReadJson("series/" + uuid_);
+      JsonArray instances = (JsonArray) series.get("Instances");
       for (int i = 0; i < instances.size(); i++)
       {
         String uuid = (String) instances.get(i);
-        JSONObject instance = (JSONObject) orthanc_.ReadJson("instances/" + uuid);
-        Long index = (Long) instance.get("IndexInSeries");
-        String s;
-        if (index == null)
+        JsonObject instance = (JsonObject) orthanc_.ReadJson("instances/" + uuid);
+
+	// Addition by Sebastien Jodogne
+	if (instance.containsKey("IndexInSeries"))
         {
-          s = uuid;
+	    children.add(new MyTreeNode(orthanc_, ResourceType.INSTANCE, uuid,
+					instance.getString("IndexInSeries")));
         }
         else
         {
-          s = String.valueOf(index);
-        }
-
-        children.add(new MyTreeNode(orthanc_, ResourceType.INSTANCE, uuid, s));
+	    // No ordering of instances is available for this series:
+	    // Use the UUID as the index of the instance.
+	    children.add(new MyTreeNode(orthanc_, ResourceType.INSTANCE, uuid, uuid));
+	}
       }
 
       return children;
@@ -556,12 +557,12 @@ public class SelectImageDialog extends J
       // https://stackoverflow.com/a/13109632/881731
       String decoded = OrthancConnection.DecodeBase64(s);
       
-      JSONArray config = (JSONArray) JSONValue.parse(decoded);
+      JsonArray config = Jsoner.deserialize(decoded, (JsonArray) null);
       if (config != null)
       {
         for (int i = 0; i < config.size(); i++)
         {
-          AddOrthancServer(OrthancConnection.Unserialize((JSONObject) config.get(i)));
+          AddOrthancServer(OrthancConnection.Unserialize((JsonObject) config.get(i)));
         }
       }
     }
@@ -569,7 +570,7 @@ public class SelectImageDialog extends J
 
   public String Serialize()
   {
-    JSONArray servers = new JSONArray();
+    JsonArray servers = new JsonArray();
 
     for (int i = 0; i < root_.getChildCount(); i++)
     {
@@ -577,7 +578,7 @@ public class SelectImageDialog extends J
       servers.add(node.GetConnection().Serialize());
     }
 
-    String config = servers.toJSONString();
+    String config = servers.toJson();
 
     // https://stackoverflow.com/a/13109632/881731
     return new String(Base64.getEncoder().encode(config.getBytes()));
